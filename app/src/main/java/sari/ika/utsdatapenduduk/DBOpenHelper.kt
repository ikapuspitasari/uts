package sari.ika.utsdatapenduduk

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context : Context):SQLiteOpenHelper(context,DB_Name,null,DB_Ver) {

    override fun onCreate(db: SQLiteDatabase?) {
        val tNamaKel = "create table NamaKel (Nokk text primary key, nama text not null, id_rt int not null)"
        val tRT = "create table rt(id_rt integer primary key autoincrement, nama_rt text not null)"
        val insRT = "insert into rt (nama_rt) values('01'),('02'),('03')"
        db?.execSQL(tNamaKel)
        db?.execSQL(tRT)
        db?.execSQL(insRT)

    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

    companion object {
        val DB_Name = "kepala keluarga"
        val DB_Ver = 1
    }
}