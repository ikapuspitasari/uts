package sari.ika.utsdatapenduduk

import android.app.AlertDialog
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_pdd.*
import kotlinx.android.synthetic.main.frag_data_pdd.view.*
import kotlinx.android.synthetic.main.frag_data_pdd.view.spinner

class FragmentPdd : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {

    override fun onNothingSelected(parent: AdapterView<*>?) {
        spinner.setSelection(0,true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c : Cursor = spAdapter.getItem(position) as Cursor
        namaRT = c.getString(c.getColumnIndex("_id"))
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnDeletepdd ->{

            }
            R.id.btnUpdatepdd ->{

            }
            R.id.btnInsertpdd ->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
            R.id.btnCari ->{
                showDataPdd(edNamaKel.text.toString())
            }
        }
    }

    lateinit var thisParent : MainActivity
    lateinit var lsAdapter : ListAdapter
    lateinit var spAdapter: SimpleCursorAdapter
    lateinit var dialog : AlertDialog.Builder
    lateinit var v : View
    var namaPdd : String = ""
    var namaRT : String = ""
    lateinit var db : SQLiteDatabase
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_data_pdd
            ,container,false)
        db = thisParent.getDbObject()
        dialog = AlertDialog.Builder(thisParent)
        v.btnDeletepdd.setOnClickListener(this)
        v.btnInsertpdd.setOnClickListener(this)
        v.btnUpdatepdd.setOnClickListener(this)
        v.spinner.onItemSelectedListener = this
        v.btnCari.setOnClickListener(this)

        return v
    }

    override fun onStart() {
        super.onStart()
        showDataPdd("")
        showDataRt()
    }

    fun showDataPdd(namaPdd : String){
        var sql = ""
        if(!namaPdd.trim().equals("")){
            sql = "select m.nok as _id, m.nama, p.nama_rt from pdd m, rt p " +
                    "where m.id_rt = p.id_rt and m.nama like '%$namaPdd%'"
        }else{
            sql = "select m.nok as _id, m.nama, p.nama_rt from pdd m, rt p " +
                    "where m.id_rt = p.id_rt order by m.nama asc"
        }
        val c : Cursor = db.rawQuery(sql,null)
        lsAdapter = SimpleCursorAdapter(thisParent,R.layout.item_data_pdd,c,
            arrayOf("_id","nama","nama_rt"), intArrayOf(R.id.txNkk, R.id.txNamaKel, R.id.txRt),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsPdd.adapter = lsAdapter
    }

    fun showDataRt(){
        val c : Cursor = db.rawQuery("select nama_rt as _id from rt order by nama_rt asc", null)
        spAdapter = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spinner.adapter = spAdapter
        v.spinner.setSelection(0)
    }

    fun insertDataPdd(nok : String, namaPdd: String, id_rt : Int){
        var sql = "insert into pdd(nok, namapdd, id_rt) values (?,?,?)"
        db.execSQL(sql, arrayOf(nok,namaPdd,id_rt))
        showDataPdd("")
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        var sql = "select id_rt from rt where nama_rt='$namaRT'"
        val c : Cursor = db.rawQuery(sql,null)
        if(c.count>0){
            c.moveToFirst()
            insertDataPdd(v.edNkk.text.toString(), v.edNamaKel.text.toString(),
                c.getInt(c.getColumnIndex("id_rt")))
            v.edNkk.setText("")
            v.edNamaKel.setText("")
        }
    }




}