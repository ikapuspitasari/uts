package sari.ika.utsdatapenduduk

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_rt.view.*
import kotlinx.android.synthetic.main.item_data_rt.*


class FragmentRT : Fragment(),View.OnClickListener {
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInRt ->{
                builder.setTitle("Konfirmasi").setMessage("Data yang akan dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }
            R.id.btnDelRt ->{
                builder.setTitle("Konfirmasi").setMessage("Yakin akan menghapus data ini?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnDeletedDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }
            R.id.btnUpRt ->{
                builder.setTitle("Konfirmasi").setMessage("Data yang akan dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnUpdateDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }
        }
    }

    lateinit var thisParent : MainActivity
    lateinit var db : SQLiteDatabase
    lateinit var adapter : ListAdapter
    lateinit var v : View
    lateinit var builder : AlertDialog.Builder
    var idRt : String = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        thisParent = activity as MainActivity
        db = thisParent.getDbObject()

        v =  inflater.inflate(R.layout.frag_data_rt,container,false)
        v.btnUpRt.setOnClickListener(this)
        v.btnInRt.setOnClickListener(this)
        v.btnDelRt.setOnClickListener(this)
        builder = AlertDialog.Builder(thisParent)
        v.lsRt.setOnItemClickListener(itemClick)

        return v
    }

    fun showDataRt(){
        val cursor : Cursor = db.query("Rt", arrayOf("nama_rt", "id_rt as _id"),
            null, null, null, null, "nama_rt asc")
        adapter = SimpleCursorAdapter(thisParent,R.layout.item_data_rt,cursor,
            arrayOf("_id", "nama_rt"), intArrayOf(R.id.txlRt, R.id.txNamaRt),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsRt.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        showDataRt()
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c : Cursor = parent.adapter.getItem(position) as Cursor
        idRt = c.getString(c.getColumnIndex("_id"))
        v.edRt.setText(c.getString(c.getColumnIndex("nama_rt")))
    }

    fun insertDataRt(namaRT : String){
        var cv : ContentValues = ContentValues()
        cv.put("nama_rt",namaRT)
        db.insert("rt",null,cv)
        showDataRt()
    }

    fun  updateDataRt(namaRT: String, idRt: String){
        var cv : ContentValues = ContentValues()
        cv.put("nama_rt",namaRT)
        db.update("rt",cv,"id_rt = $idRt",null)
        showDataRt()
    }

    fun deleteDataProdi(idRt : String){
        db.delete("rt","id_rt = $idRt", null)
        showDataRt()
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        insertDataRt(v.edRt.text.toString())
        v.edRt.setText("")
    }

    val btnUpdateDialog = DialogInterface.OnClickListener { dialog, which ->
        updateDataRt(v.edRt.text.toString(),idRt)
        v.edRt.setText("")
    }

    val btnDeletedDialog = DialogInterface.OnClickListener { dialog, which ->
        deleteDataProdi(idRt)
        v.edRt.setText("")
    }
}